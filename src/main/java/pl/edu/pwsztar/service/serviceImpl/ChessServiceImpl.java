package pl.edu.pwsztar.service.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.chess.RulesOfGame;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.domain.dto.IntMovePositionsDto;
import pl.edu.pwsztar.domain.mapper.Mapper;
import pl.edu.pwsztar.service.ChessService;

@Service
public class ChessServiceImpl implements ChessService {

    private Mapper<IntMovePositionsDto,FigureMoveDto> moveToPositionsMapper;
    private RulesOfGame bishop;
    private RulesOfGame knight;
    private RulesOfGame king;
    private RulesOfGame rock;
    private RulesOfGame queen;
    private RulesOfGame pawn;

    @Autowired
    public ChessServiceImpl(@Qualifier("Bishop") RulesOfGame bishop,
                            @Qualifier("Knight") RulesOfGame knight,
                            @Qualifier("King") RulesOfGame king,
                            @Qualifier("Rock") RulesOfGame rock,
                            @Qualifier("Queen") RulesOfGame queen,
                            @Qualifier("Pawn") RulesOfGame pawn,
                            Mapper<IntMovePositionsDto,FigureMoveDto> moveToPositionsMapper) {
        this.bishop = bishop;
        this.knight = knight;
        this.king = king;
        this.rock = rock;
        this.queen = queen;
        this.pawn = pawn;
        this.moveToPositionsMapper = moveToPositionsMapper;
    }

    @Override
    public boolean isCorrectMove(FigureMoveDto figureMoveDto){
        IntMovePositionsDto intMovePositionsDto = moveToPositionsMapper.convert(figureMoveDto);

        switch (figureMoveDto.getType()){
            case KING:
                return  king.isCorrectMove(intMovePositionsDto.getXStart(),
                        intMovePositionsDto.getYStart(),
                        intMovePositionsDto.getXEnd(),
                        intMovePositionsDto.getYEnd());
            case PAWN:
                return  pawn.isCorrectMove(intMovePositionsDto.getXStart(),
                        intMovePositionsDto.getYStart(),
                        intMovePositionsDto.getXEnd(),
                        intMovePositionsDto.getYEnd());
            case ROCK:
                return  rock.isCorrectMove(intMovePositionsDto.getXStart(),
                        intMovePositionsDto.getYStart(),
                        intMovePositionsDto.getXEnd(),
                        intMovePositionsDto.getYEnd());
            case QUEEN:
                return  queen.isCorrectMove(intMovePositionsDto.getXStart(),
                        intMovePositionsDto.getYStart(),
                        intMovePositionsDto.getXEnd(),
                        intMovePositionsDto.getYEnd());
            case KNIGHT:
                return  knight.isCorrectMove(intMovePositionsDto.getXStart(),
                        intMovePositionsDto.getYStart(),
                        intMovePositionsDto.getXEnd(),
                        intMovePositionsDto.getYEnd());
            case BISHOP:
                return  bishop.isCorrectMove(intMovePositionsDto.getXStart(),
                        intMovePositionsDto.getYStart(),
                        intMovePositionsDto.getXEnd(),
                        intMovePositionsDto.getYEnd());
        }

        return false;
    }
}
