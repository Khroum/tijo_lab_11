package pl.edu.pwsztar.domain.mapper;

public interface Mapper<T,F> {
    T convert(F from);
}