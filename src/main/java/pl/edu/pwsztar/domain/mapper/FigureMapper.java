package pl.edu.pwsztar.domain.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.domain.dto.IntMovePositionsDto;

@Component
public class FigureMapper implements Mapper<IntMovePositionsDto, FigureMoveDto> {

    @Override
    public IntMovePositionsDto convert(FigureMoveDto from) {
        int xStart = from.getStart().charAt(0);
        int yStart = from.getStart().charAt(2);

        int xEnd = from.getDestination().charAt(0);
        int yEnd = from.getDestination().charAt(2);

        return new IntMovePositionsDto(xStart, yStart, xEnd, yEnd);
    }
}
