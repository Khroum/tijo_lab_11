package pl.edu.pwsztar.domain.chess;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

public interface RulesOfGame {

    /**
     * Metoda zwraca true, tylko gdy przejscie z polozenia
     * (xStart, yStart) na (xEnd, yEnd) w jednym ruchu jest zgodne
     * z zasadami gry w szachy
     */
    boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd);
    static boolean isEndStart(int xStart, int yStart, int xEnd, int yEnd) {
        if(xStart == xEnd && yStart == yEnd) {
            return true;
        }
        return false;
    }

    @Component
    @Qualifier("Bishop")
    class Bishop implements RulesOfGame {

        @Override
        public boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd) {
            if (isEndStart(xStart, yStart, xEnd, yEnd)) {
                return false;
            }

            return Math.abs(xEnd - xStart) == Math.abs(yEnd - yStart);
        }
    }


    @Component
    @Qualifier("Knight")
    class Knight implements RulesOfGame {

        @Override
        public boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd) {
            if (isEndStart(xStart, yStart, xEnd, yEnd)) {
                return false;
            }

            int diffX = Math.abs(xEnd-xStart);
            int diffY = Math.abs(yEnd-yStart);

            return (diffX == 2 && diffY == 1) || (diffX == 1 && diffY == 2);
        }
    }


    @Component
    @Qualifier("King")
    class King implements RulesOfGame {

        @Override
        public boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd) {
            if (isEndStart(xStart, yStart, xEnd, yEnd)) {
                return false;
            }

            int diffX = Math.abs(xEnd-xStart);
            int diffY = Math.abs(yEnd-yStart);

            return (diffX == 1 && diffY == 0) || (diffX == 0 && diffY == 1) || (diffX == 1 && diffY == 1);
        }
    }


    @Component
    @Qualifier("Queen")
    class Queen implements RulesOfGame {

        @Override
        public boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd) {
            if (isEndStart(xStart, yStart, xEnd, yEnd)) {
                return false;
            }

            return  Math.abs(xEnd - xStart) == Math.abs(yEnd - yStart) || xStart == xEnd || yStart == yEnd ;
        }
    }


    @Component
    @Qualifier("Rock")
    class Rock implements RulesOfGame {

        @Override
        public boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd) {
            if (isEndStart(xStart, yStart, xEnd, yEnd)) {
                return false;
            }

            return (xStart == xEnd) || (yStart == yEnd);
        }
    }


    @Component
    @Qualifier("Pawn")
    class Pawn implements RulesOfGame {
        static boolean firstMove = true;

        @Override
        public boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd) {
            if (isEndStart(xStart, yStart, xEnd, yEnd)) {
                return false;
            }

            if (isFirst(xStart, yStart, xEnd, yEnd)) {
                return true;
            }

            return yEnd - yStart == 1 && xStart == xEnd;
        }

        private boolean isFirst(int xStart, int yStart, int xEnd, int yEnd) {
            if(firstMove == true){
                if((yEnd - yStart == 2 && xStart == xEnd) || (yEnd - yStart == 1 && xStart == xEnd)){
                    firstMove = false;
                    return true;
                }
            }
            return false;
        }
    }

    // TODO: Prosze dokonczyc implementacje kolejnych figur szachowych: Knight, King, Queen, Rock, Pawn
    // TODO: Prosze stosowac zaproponowane nazwy klas !!! (Prowadzacy zajecia posiada wlasne testy)
    // TODO: Kazda klasa powinna implementowac interfejs RulesOfGame
}
