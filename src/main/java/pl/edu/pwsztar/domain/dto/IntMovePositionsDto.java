package pl.edu.pwsztar.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public class IntMovePositionsDto {
    private  int xStart;
    private  int yStart;
    private  int xEnd;
    private  int yEnd;
}
